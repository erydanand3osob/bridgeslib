package pl.localbridge.bridgeslib;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bridge")
public class BridgeController {
    @Autowired
    BridgesRepository bridgesRepository;

    @GetMapping("/test")
    public int test() {

        return 1;
    }

    @GetMapping("")
    public List<Bridge> getAll() {
        return bridgesRepository.getAll();
    }

    @GetMapping("/{bridge_id}")
    public Bridge getById(@PathVariable("bridge_id") int bridge_id) {
        return bridgesRepository.getById(bridge_id);
    }
    @PostMapping("")
    public @ResponseBody String add(@RequestBody List<Bridge> bridges) {
        return bridgesRepository.save(bridges);
    }
    @PutMapping("/{bridge_id}")
    public int update(@PathVariable("bridge_id") int bridge_id, @RequestBody Bridge updateBridge){

        Bridge bridge = bridgesRepository.getById(bridge_id);
        if (bridge != null){
            //System.console().writer().println(bridge);
            bridge.setBridgename(updateBridge.getBridgename());
            bridge.setCrosses(updateBridge.getCrosses());
            bridge.setYear_opened(updateBridge.getYear_opened());
            bridge.setNotes(updateBridge.getNotes());
            //System.console().writer().println(bridge);
            //System.console().writer().println(updateBridge);

            bridgesRepository.update(bridge);
            //return bridge.toString();
            return 1;
        } else{
            return -1;
        }

    }

    @PatchMapping("/{bridge_id}")
    public int partiallyUpdate(@PathVariable("bridge_id") int bridge_id, @RequestBody Bridge updateBridge) {

        Bridge bridge = bridgesRepository.getById(bridge_id);
        if (bridge != null) {
            //System.console().writer().println(bridge);
            if (
                    updateBridge.getBridgename() != null
            ) {
                bridge.setBridgename(updateBridge.getBridgename());
            }
            if (
                    updateBridge.getCrosses() != null
            ) {
                bridge.setCrosses(updateBridge.getCrosses());
            }

            if (
                    updateBridge.getYear_opened() > 0
            ) {
                bridge.setYear_opened(updateBridge.getYear_opened());
            }
            if (updateBridge.getNotes() != null) {
                bridge.setNotes(updateBridge.getNotes());
            }


            //System.console().writer().println(bridge);
            //System.console().writer().println(updateBridge);

            bridgesRepository.update(bridge);
            //return bridge.toString();
            return 1;
        } else {
            return -1;
        }
    }

    @DeleteMapping("/{bridge_id}")
    public int delete(@PathVariable ("bridge_id") int bridge_id){
        return bridgesRepository.delete(bridge_id);
    }
}

