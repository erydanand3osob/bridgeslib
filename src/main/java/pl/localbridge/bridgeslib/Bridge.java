package pl.localbridge.bridgeslib;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Bridge {
    private int bridge_id;
    private String bridgename;
    private String crosses;
    private int year_opened;
    private String notes;
}
