package pl.localbridge.bridgeslib;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class BridgesRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Bridge> getAll() {
        return jdbcTemplate.query(
                "SELECT bridge_id, bridgename, crosses, year_opened, notes FROM bridges",
                BeanPropertyRowMapper.newInstance(Bridge.class));
    }

    public Bridge getById(int bridge_id) {
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT bridge_id, bridgename, crosses, year_opened, notes FROM bridges WHERE " + "bridge_id = ?",
                    BeanPropertyRowMapper.newInstance(Bridge.class), bridge_id);
        }
        catch (DataRetrievalFailureException e){
            return null;
        }
    }
    public int update(Bridge bridge){
        try{
            return jdbcTemplate.update(
                    "UPDATE bridges SET bridgename=?, crosses=?, year_opened=?::int, notes=?::text WHERE bridge_id=? ",
                    bridge.getBridgename(), bridge.getCrosses() ,bridge.getYear_opened() ,bridge.getNotes(), bridge.getBridge_id());


        }
        catch(Exception e){
            e.printStackTrace();
            //return e.getMessage();
            return 0;
        }
    }

    public String save(List<Bridge> bridges) {
        try{
            bridges.forEach(bridge -> jdbcTemplate
                    .update(
                            "INSERT INTO bridges (bridge_id, bridgename, crosses, year_opened, notes) VALUES (?, ?, ?, ?, ?) ",
                            bridge.getBridge_id(), bridge.getBridgename(), bridge.getCrosses(), bridge.getYear_opened(), bridge.getNotes()
                    ));


            return "Saved";
        }
catch(Exception e){
    e.printStackTrace();
            return e.getMessage();
        }
    }

    public int delete(int bridge_id){
        return jdbcTemplate.update("DELETE FROM bridges WHERE bridge_id=?", bridge_id);
    }
}
