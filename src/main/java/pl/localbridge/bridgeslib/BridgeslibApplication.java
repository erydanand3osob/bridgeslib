package pl.localbridge.bridgeslib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BridgeslibApplication {

    public static void main(String[] args) {
        SpringApplication.run(BridgeslibApplication.class, args);
    }

}
